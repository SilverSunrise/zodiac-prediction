package cliz.odi.predictionzodiac

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.SnapHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row_post.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sign: ArrayList<CardArray> = ArrayList()

        sign.add(CardArray(R.drawable.aries, "0"))
        sign.add(CardArray(R.drawable.taurus, "1"))
        sign.add(CardArray(R.drawable.twins, "2"))
        sign.add(CardArray(R.drawable.crayfish, "3"))

        sign.add(CardArray(R.drawable.lion, "4"))
        sign.add(CardArray(R.drawable.virgo, "5"))
        sign.add(CardArray(R.drawable.libra, "6"))
        sign.add(CardArray(R.drawable.scorpio, "7"))

        sign.add(CardArray(R.drawable.sagittarius, "8"))
        sign.add(CardArray(R.drawable.capricorn, "9"))
        sign.add(CardArray(R.drawable.aquarius, "10"))
        sign.add(CardArray(R.drawable.fish, "11"))


        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = ZodiacAdapter(sign)
        val snapHelper: SnapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(recyclerView)


        val intent = Intent(this, ShowCardActivity::class.java)
        radio_group.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_color1 -> {
                    intent.putExtra("color", "Yellow")
                }
                R.id.rb_color2 -> {
                    intent.putExtra("color", "Blue")
                }
                R.id.rb_color3 -> {
                    intent.putExtra("color", "Green")
                }
                R.id.rb_color4 -> {
                    intent.putExtra("color", "Purple")
                }
            }
            when (tv_id.text) {
                "0" -> {
                    intent.putExtra("sign", "Aries")
                }
                "1" -> {
                    intent.putExtra("sign", "Taurus")
                }
                "2" -> {
                    intent.putExtra("sign", "Gemini")
                }
                "3" -> {
                    intent.putExtra("sign", "Cancer")
                }
                "4" -> {
                    intent.putExtra("sign", "Leo")
                }
                "5" -> {
                    intent.putExtra("sign", "Virgo")
                }
                "6" -> {
                    intent.putExtra("sign", "Libra")
                }
                "7" -> {
                    intent.putExtra("sign", "Scorpio")
                }
                "8" -> {
                    intent.putExtra("sign", "Sagittarius")
                }
                "9" -> {
                    intent.putExtra("sign", "Capricorn")
                }
                "10" -> {
                    intent.putExtra("sign", "Aquarius")
                }
                "11" -> {
                    intent.putExtra("sign", "Pisces")
                }


            }
            startActivity(intent)
        }
    }
}