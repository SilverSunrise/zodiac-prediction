package cliz.odi.predictionzodiac

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_show_card.*

class ShowCardActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_card)
        val sign = intent.getStringExtra("sign")
        val color = intent.getStringExtra("color")
        tv_result.text = "Your choice is $sign and $color color. \nHere is your card!"

        val imagesDiamonds: MutableList<Int> = mutableListOf(
            R.drawable.b_6,
            R.drawable.b_7,
            R.drawable.b_8,
            R.drawable.b_9,
            R.drawable.b_10,
            R.drawable.b_val,
            R.drawable.b_dam,
            R.drawable.b_kor,
            R.drawable.b_tuz
        )
        val imagesSpades: MutableList<Int> = mutableListOf(
            R.drawable.p_6,
            R.drawable.p_7,
            R.drawable.p_8,
            R.drawable.p_9,
            R.drawable.p_10,
            R.drawable.p_val,
            R.drawable.p_dam,
            R.drawable.p_kor,
            R.drawable.p_tuz
        )
        val imagesClubs: MutableList<Int> = mutableListOf(
            R.drawable.t_6,
            R.drawable.t_7,
            R.drawable.t_8,
            R.drawable.t_9,
            R.drawable.t_10,
            R.drawable.t_val,
            R.drawable.t_dam,
            R.drawable.t_kor,
            R.drawable.t_tuz
        )

        val imagesHearts: MutableList<Int> = mutableListOf(
            R.drawable.c_6,
            R.drawable.c_7,
            R.drawable.c_8,
            R.drawable.c_9,
            R.drawable.c_10,
            R.drawable.c_val,
            R.drawable.c_dam,
            R.drawable.c_kor,
            R.drawable.c_tuz
        )


        when (color) {
            "Yellow" -> {
                val ran = (0..9).random()
                iv_card.setImageResource(imagesClubs[ran])
            }
            "Blue" -> {
                val ran = (0..9).random()
                iv_card.setImageResource(imagesDiamonds[ran])
            }
            "Green" -> {
                val ran = (0..9).random()
                iv_card.setImageResource(imagesHearts[ran])
            }
            "Purple" -> {
                val ran = (0..9).random()
                iv_card.setImageResource(imagesSpades[ran])
            }
        }

        btn_restart.setOnClickListener {
            val back = Intent(this, MainActivity::class.java)
            startActivity(back)
        }
    }
}