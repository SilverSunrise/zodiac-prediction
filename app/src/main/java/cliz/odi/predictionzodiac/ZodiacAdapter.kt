package cliz.odi.predictionzodiac

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_post.view.*

class ZodiacAdapter(private val sign: ArrayList<CardArray>) :
    RecyclerView.Adapter<ZodiacAdapter.ZodiacViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ZodiacViewHolder {
        val itemSign =
            LayoutInflater.from(parent.context).inflate(R.layout.row_post, parent, false)
        return ZodiacViewHolder(itemSign)
    }

    override fun onBindViewHolder(holder: ZodiacViewHolder, position: Int) {
        val model: CardArray = sign[position]
        holder.image.setImageResource(model.iconImage)
        holder.name.text = position.toString()


    }

    override fun getItemCount(): Int {
        return sign.size
    }

    class ZodiacViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val image: ImageView = item.image
        val name: TextView = item.findViewById(R.id.tv_id)
    }

}